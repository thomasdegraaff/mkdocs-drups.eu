<style>
  .md-typeset h1 {
    display: none;
  }
</style>
Lando comes with a [commandline client](https://docs.lando.dev/basics/usage.html). Drups adds some extra lando commands to manage your development environment. Lando commands are to be executed in your project branch folder.

## General lando commands
* To remove your lando dev environment and lose unstored database and files.
```
lando destroy
```
* Get information about your local development site.
```
lando info
```
* Use mysql.
```
lando mysql
```
* If you want to rebuild your app (some changes of lando.yml require this).
```
lando rebuild
```
* To start your stopped dev environment
```
lando start
```
* To stop your local dev site and free memory usage.
```
lando stop
```
* Get into app container.
```
lando ssh -u root -s tooling
```
* To do something else.
```
lando --help
lando [command] --help
```
## Drups lando commands
* Clear Varnish cache and opcache in local deployment.
```
lando cc-local
```
* Clear Varnish cache Drupal cache and opcache in remote deployment.
```
lando cc-remote
```
* Check custom module code.
```
lando code-check
```
* Fix custom module code.
```
lando code-fix
```
* Execute composer in the container running your local dev site, for example to install a Drupal module or theme.
```
lando composer require drupal/module_name
```
* Export your development site configuration.
```
lando config-export
```
* Import configuration in your local development site.
```
lando config-import
```
* Destroy remote deployment.
```
lando destroy-remote
```
* Use Drush in your local deployment, for example to install a Drupal module.
```
lando drush pm:enable module_name
```
* List remote deployments in current project.
```
lando list-remote
```
* Store data from your local development site. (The data will be stored in the storage folder. The file names will contain the locally deployed commit id. Existing data with the same id will be overwritten.)
```
lando pull-data-local
```
* Store data from the site running remotely in the cluster. (The data will be stored in the project storage folder. The file names will contain the remote deployed commit id. Existing data with the same id will be overwritten.)
```
lando pull-data-remote
```
* Push stored data to your local development site. (You will need existing data stored with the same commit id that is checked out locally.)
```
lando push-data-local
```
* Push stored data to the site running in the cluster. (You will need existing data stored with the same commit id that is deployed remotely.)
```
lando push-data-remote
```
* Rebuild, reinitialize remote deployment.
```
lando rebuild-remote
```
* Update Drupal core in your local development site.
```
lando update-core
```
* Update Drupal modules in your local development site.
```
lando update-modules
```
* Update remote deployment after pushing new code to the repo.
```
lando update-remote
```