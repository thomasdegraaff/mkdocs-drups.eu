# Advanced topics

## Update Docker images
You can find the most current docker image build definitions in the ```drups/docker-images``` folder in the [Drups repository](https://gitlab.com/thomasdegraaff/drups).
```
git clone https://gitlab.com/thomasdegraaff/drups.git drups-repo
```

The Dockerhub image url's can be found in the ```drups-images``` file in the repo root folder.
``` title="drups-images"
# Drups docker images.
php_image='thomasdegraaff/drups-php:0.1'
lando_php_image='thomasdegraaff/drups-lando-php:0.1'
drupal_tools_image='thomasdegraaff/drups-drupal-tools:0.1'
lando_tools_image='thomasdegraaff/drups-lando-tools:0.1'
nginx_image='thomasdegraaff/drups-nginx:0.1'
varnish_image='thomasdegraaff/drups-varnish:0.1'
redis_image='thomasdegraaff/drups-redis:0.1'
mailhog_image='thomasdegraaff/drups-mailhog:0.1'
mariadb_image='mariadb:10.3'
```

You can pull the most recent image.
```
docker image pull thomasdegraaff/drups-lando-tools:0.1
```

Do some security checks ([trivy](https://github.com/aquasecurity/trivy)).
```
trivy image thomasdegraaff/drups-lando-tools:0.1
```
 When you find security issues, you can update the image definition so it uses more recent versions of the insecure software. The ```drupal-tools``` image definition doesn't contain hard coded versions, so it's just a question of rebuilding the image to get the most recent versions.

 First you need to setup a multiarch Docker image build environment.
```
docker run --rm --privileged docker/binfmt:820fdd95a9972a5308930a2bdfb8573dd4447ad3
cat /proc/sys/fs/binfmt_misc/qemu-aarch64
docker buildx create --name mybuilder
docker buildx use mybuilder
docker buildx inspect --bootstrap
```
Then you can build a new version of the image.
```
cd drups-repo/drups/docker-images/drupal-tools
docker buildx build --platform linux/amd64 --no-cache --load --tag [username]/[image-name]:test .
```
Then test the image again, and if the security issues are resolved, build the multiarch image, and push it to the remote repo.
```
docker buildx build --platform linux/arm,linux/arm64,linux/amd64 --push --tag [username]/[image-name]:[tag-name] .
```
Now edit the ```drups-images``` file in your project branch directory, update the image, and rebuild your local deployment.
```
cd your/project/branchname
lando rebuild --y
```
Check if your lando deployment build without errors, and the site is running fine. If so, rebuild your remote deployment.
```
lando rebuild remote
```

## Xdebug
todo

## Rebuild remote deployment
You might want to update your remote deployment for a couple of reasons.
* There are new Drups image versions available.
* You want to change the url of the site.
* You want to increase the storage size.
* You want to give ```php``` more memory, or allow bigger files to be uploaded.
* You made some modifications to the ```helm``` configuration used to deploy Drups, and want to update your remote deployment.
* etc.

```
cd your/project/branchname
lando rebuild-remote
```

## Test code
todo