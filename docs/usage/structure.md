# Folder structure

## Folder structure
```
.
├── composer.json                      # Specifies the Drupal installation.
├── composer.lock
├── crontab                            # Crontab runs in Drupal tools container.
├── drupal
│   ├── config
│   │   └── sync                       # Drupal config storage location.
│   ├── private_files                  # Private files storage location.
│   ├── profiles
│   │   └── drups                      # Custom Drups Drupal profile.
│   └── settings                       # Drupal settings files used.
│       ├── settings.append            # Appended to default settings file.
│       ├── settings.cluster.php       # Drupal settings for remote deployment.
│       └── settings.lando.php         # Drupal settings for local deployment.
├── drups
│   ├── config
│   │   └── drups_config               # Drups initialization variables.
│   ├── docker-images                  # Docker image build definitions.
│   │   ├── drupal-tools
│   │   ├── lando-php-fpm
│   │   ├── lando-tools
│   │   ├── mailhog
│   │   ├── nginx
│   │   ├── php-fpm
│   │   ├── redis
│   │   └── varnish
│   └── scripts                        # Scripts used in deployments.
│       ├── check_database.sh
│       ├── check_drupal.sh
│       ├── drupal_build.sh
│       ├── drupal_install.sh
│       └── includes.sh
├── drups-defaults                     # Default values for initialization.
├── drups-images                       # Defines used images.
├── drups-init                         # Drups initialization script.
├── kube
│   ├── helm
│   │   └── drups                      # Helm definition of Drups deployment.
│   │       ├── Chart.yaml
│   │       ├── templates
│   │       ├── values.yaml            # Helm values file.
│   │       └── values.yaml.orig       # Helm values template file.
│   ├── jobs
│   │   ├── drupal-build.yaml          # Build job definition.
│   │   └── drupal-build.yaml.orig     # Template used to create build job.
│   └── scripts
│       ├── drupal_build.sh            # Remote build script.
│       └── drupal_install.sh          # Remote install, update script.
├── lando
│   └── scripts
│       ├── build_steps
│       │   ├── build.sh               # Local build script.
│       │   └── import.sh              # Local import script.
│       └── tooling                    # Custom Lando command scripts.
│           ├── code_check.sh
│           ├── code_fix.sh
│           ├── config_export.sh
│           ├── config_import.sh
│           ├── destroy_remote.sh
│           ├── drups_init.sh
│           ├── includes.sh
│           ├── list_remote.sh
│           ├── pull_data_local.sh
│           ├── pull_data_remote.sh
│           ├── push_data_local.sh
│           ├── push_data_remote.sh
│           ├── rebuild_remote.sh
│           ├── update_core.sh
│           ├── update_modules.sh
│           ├── update_remote.sh
│           └── vcc_remote.sh
├── phpstan.neon                       # Php stan definition.
├── README.md
├── storage                            # Storage location for Site data.
│   ├── database-0dd4242d32.gz
│   ├── files-0dd4242d32.tar.gz
│   └── private-files-0dd4242d32.tar.gz
├── vendor                             # Drupal vendor code.
└── web                                # Drupal root directory.
    ├── core
    ├── index.php
    ├── modules
    │   ├── contrib                    # Contrib modules used in Drups.
    │   │   ├── big_pipe_sessionless
    │   │   ├── error_log
    │   │   ├── purge
    │   │   ├── purge_purger_http
    │   │   ├── queue_ui
    │   │   ├── redis
    │   │   ├── simple_sitemap
    │   │   └── warmer
    │   └── README.txt
    ├── opcache-reset.php              # Script to reset opcache.
    ├── profiles
    │   ├── drups                      # Custom Drupal installation profile.
    │   │   ├── config
    │   │   ├── drups.info.yml
    │   │   ├── drups.install
    │   │   ├── drups.links.menu.yml
    │   │   ├── drups.profile
    │   │   └── tests
    │   └── README.txt
    └── sites
        └── default
            ├── files
            ├── settings.cluster.php   # Drupal cluster settings.
            ├── settings.lando.php     # Drupal lando settings.
            ├── settings.local.php     # Default local settings.
            └── settings.php           # Drupal settings.

```
## Special files
* **settings files**
```
├── drupal
│   └── settings                       # Drupal settings files used.
│       ├── settings.append            # Appended to default settings file.
│       ├── settings.cluster.php       # Drupal settings for remote deployment.
│       └── settings.lando.php         # Drupal settings for local deployment.
```
These files define the Drupal settings for local and remote deployments. You can change these when your own project needs specific Drupal settings. These files are copied when a deployment is created. Changing the content of these files will need rebuilding a deployment to take effect.
* **drups_config**
```
├── drups
│   ├── config
│   │   └── drups_config               # Drups initialization variables.
```
When initializing Drups by executing ```drups-init``` some Drups variables are stored in this file. These variables are used for consequent Drups lando commands.
``` title="drups_config"
# General.
client_name='tom'
project_name='drupsdemo'
lando_name='drupsdemo-test'

# Url's.
domain='tomops.nl'
subdomain='test.drupsdemo'
site_url='test.drupsdemo.tomops.nl'
source_url='drupsdemo.main.tomio.nl'
lando_subdomain='test.drupsdemo'
lando_url='test.drupsdemo.lndo.site'

# Git.
git_url='https://gitlab.com/thomasdegraaff/drups-demo.git'
git_origin='origin'
git_branch='test'
source_branch='main'

# Deployment
kube_context='picluster'

# Deployment storage.
storage_class='do-block-storage'
storage_access_mode='ReadWriteOnce'
database_storage_size='2Gi'
files_storage_size='2Gi'
code_storage_size='2Gi'

# Deployment naming.
node_name='pool-4mcke2mua-cmqgp'
namespace='drupsdemo'
pod_prefix='test'
source_prefix='main'

# Deployment secrets.
drupal_admin_password='tuxae8Fi'
drupal_cron_key='ooT4Aeng'
drupal_hash_salt='aej9dahD'
mariadb_password='eenee8Ai'

# Basic authentication.
basic_authentication_user='admin'
basic_authentication_password='Ufo2ooy8'

# Mailhog.
mailhog_user='admin'
mailhog_password='Ufo2ooy8'

# Lando secrets.
lando_drupal_admin_password='drupal'
lando_drupal_cron_key='secret'
lando_drupal_hash_salt='secret'
lando_mariadb_password='drupal'

# User info.
user='tdgraaff'
user_id='1000'
group_id='1000'
```
* **docker-images**
```
├── drups
│   ├── docker-images                  # Docker image build definitions.
│   │   ├── drupal-tools
│   │   ├── lando-php-fpm
│   │   ├── lando-tools
│   │   ├── mailhog
│   │   ├── nginx
│   │   ├── php-fpm
│   │   ├── redis
│   │   └── varnish
```
The docker-images folders contain Docker build definitions that define the Docker images used in the deployments. If needed you can change these build files to [create your own images](/usage/advanced/#update-or-modify-docker-images) that meet your specific needs for a Drupal project. The ```README.md``` file in the folder contains instructions on how to build multiarch Docker images. Push the docker images to the Docker registry, and update the ```drups-images``` file in the project root before initializing Drups or rebuilding a Drups deployment.
* **drups-defaults**
```
├── drups-defaults                     # Default values for initialization.
```
Default values used when initialzing Drups. Modify these values to your needs. Beware! This file will be stored in git, so make sure not to set secrets in this file when using a public repository.
``` title="drups-defaults"
# Edit to change drups defaults.

# General.
default_client='tom'
default_domain='tomio.nl'

# Deployment secrets (when left empty they will be asked for on drups initialization).
drupal_admin_password=''
drupal_cron_key=''
drupal_hash_salt=''
mariadb_password=''

# Lando secrets.
lando_drupal_admin_password='drupal'
lando_drupal_cron_key='secret'
lando_drupal_hash_salt='secret'
lando_mariadb_password='drupal'

# Basic authentication (leave empty to disable basic authentication).
basic_authentication_user=''
basic_authentication_password=''

# Mailhog (when left empty mailhog will not be deployed remotely).
mailhog_user=''
mailhog_password=''

# Storage sizes.
code_storage_size='1Gi'
files_storage_size='1Gi'
database_storage_size='1Gi'

# Php settings.
php_memory_limit_build='256M'
php_memory_limit_tooling='256M'
php_memory_limit_fpm='256M'

php_max_execution_time_build='0'
php_max_execution_time_tooling='0'
php_max_execution_time_fpm='60'

php_upload_max_filesize_build='8M'
php_upload_max_filesize_tooling='8M'
php_upload_max_filesize_fpm='8M'

php_post_max_size_build='8M'
php_post_max_size_tooling='8M'
php_post_max_size_fpm='8M'

# Varnish.
varnish_size='400M'

# Xdebug.
xdebug_port='9003'

# Content security policy.
content_security_policy="default-src 'none'; script-src 'self' 'unsafe-inline'; connect-src 'self'; img-src 'self' data:; style-src 'self' 'unsafe-inline'; font-src 'self'; manifest-src 'self'; base-uri 'self';"
```
* **drups-images**
```
├── drups-images                       # Defines used images.
```
The ```drups-images``` file defines what images are used when initializing Drups or rebuilding a deployment. When executing ```lando rebuild-remote``` new default image versions will be automaticly downloaded from the Drups repository, but you can change them manually during initialization.
``` title="drups-images"
# Drups docker images.
php_image='thomasdegraaff/drups-php:0.1'
lando_php_image='thomasdegraaff/drups-lando-php:0.1'
drupal_tools_image='thomasdegraaff/drups-drupal-tools:0.1'
lando_tools_image='thomasdegraaff/drups-lando-tools:0.1'
nginx_image='thomasdegraaff/drups-nginx:0.1'
varnish_image='thomasdegraaff/drups-varnish:0.1'
redis_image='thomasdegraaff/drups-redis:0.1'
mailhog_image='thomasdegraaff/drups-mailhog:0.1'
mariadb_image='mariadb:10.3'
```
*  **helm**
[Helm](https://helm.sh/) is used for deployments in the Kubernetes cluster. You can use the ```helm``` commandline commands to inspect and delete the deployment, or use ```lando destroy-remote``` or ```lando list-remote```.<br/>You can find the ```helm``` configuration here.
```
├── kube
│   ├── helm
│   │   └── drups                      # Helm definition of Drups deployment.
│   │       ├── Chart.yaml
│   │       ├── templates
│   │       ├── values.yaml            # Helm values file.
│   │       └── values.yaml.orig       # Helm values template file.
```
* **phpstan.neon**
```
├── phpstan.neon                       # Php stan definition.
```
This file contains the ```phpstan``` configuration that is used when executing ```lando code-check```.
``` title="phpstan.neon"
parameters:
  level: 8
  drupal:
    drupal_root: /app/web
includes:
  - /composer/vendor/mglaman/phpstan-drupal/extension.neon
  - /composer/vendor/phpstan/phpstan-deprecation-rules/rules.neon
  - /composer/vendor/phpstan/phpstan-strict-rules/rules.neon
```
* **storage**
```
├── storage                            # Storage location for Site data.
│   ├── database-0dd4242d32.gz
│   ├── files-0dd4242d32.tar.gz
│   └── private-files-0dd4242d32.tar.gz

```
This folder contains the site data storage files. If you want to push data to a deployment, the deployment the data was retrieved from should have the same commit id as the deployment the data is pushed to. Drups stores the git commit id in the data file names when pulling data from a deployment. Before pushing data, Drups checks if data storage exists that corresponds to the target deployment git id.
* **opcache-reset.php**
```
└── web                                # Drupal root directory.
    ├── opcache-reset.php              # Script to reset opcache.
```
When you make changes to ```settings.php```, these changes will not become active until you clear the ```opcache```. You can do this manually by visiting ```https://your.lndo.site/opcache-reset.php``` or by executing ```lando vcc-local``` or ```lando vcc-remote```.


