# Workflow
<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/703326351?h=604449cc27&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;" title="drups-workflow"></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

## Basic Drups workflow

1. Create a project folder on your workstation.
1. Within the project folder clone the [Drups repo](https://gitlab.com/thomasdegraaff/drups) in the ```main``` branch folder.
```
.
└── drupsdemo
    └── main
        ├── drupal
        ├── drups
        ├── drups-defaults
        ├── drups-init
        ├── kube
        ├── lando
        ├── .lando.yml.orig
        ├── README.md
        ├── storage
        ├── vendor
        └── web
```
1. Make sure the ```main``` branch is checked out.
1. Execute ```drups-init``` and answer some questions to initialize the project.
> A base Drupal site for the ```main``` branch will be deployed in the chosen cluster and then published on the internet.<br>
```
https://main.drupsdemo.thomasdegraaff.eu
```
> Your lando environment will be configured to create a local clone of the ```main``` branch site to work on.
```
https://drupsdemo.main.lndo.site
```
1. Execute ```lando start``` to startup the local deployment of the site.
> Now you have both a remote and a local deployment of the ```main``` branch website.

1. Repeat step 2-5 for other branches you want to work on (change ```main``` to the name of the branch you want to work on).
> You will need a separate clone in a separate folder for each branch you want to work on. Each branch folder will correspond to a different lando and remote deployment.
```
.
└── drupsdemo
    ├── feature # Checked out the feature branch.
    │   ├── drupal
    │   └── ...
    ├── main # Checked out the main branch.
    │   ├── drupal
    │   └── ...
    └── staging # Checked out the staging branch.
        ├── drupal
        └── ...
```
```
public https://feature.drupsdemo.thomasdegraaff.eu
local  https://feature.drupsdemo.lndo.site

public https://main.drupsdemo.thomasdegraaff.eu
local  https://main.drupsdemo.lndo.site

public https://staging.drupsdemo.thomasdegraaff.eu
local  https://staging.drupsdemo.lndo.site
```
1. Go into a branch folder to do some development on that branch.
1. Push your work to the remote git repo when you are ready.
1. Now execute ```lando update-remote``` to update the remote site with the new code (so you can show the new feature to the client).
1. Optionally you can push content (database and files) to the site deployed in the cluster. Execute ```lando pull-data-local``` followed by ```lando push-data-remote``` to accomplish this.

If you want to continue with the tutorial you first need to setup a Drups compatible Kubernetes cluster.

[setup a managed cluster](/managed-cluster/workstation/){ .md-button }
[Build your Pi cluster](/pi-cluster/demo/){ .md-button }
