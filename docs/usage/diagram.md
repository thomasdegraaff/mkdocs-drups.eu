# Diagram
You use Drups from your workstation and connect with your own build cluster or a Drups compatible third party managed cluster or both.

``` mermaid
graph TB
  subgraph "Pi Cluster"
    subgraph Local network
      Router
      subgraph Drups hardware
        Pi0  --- Switch
        subgraph K3s Cluster
          Switch --- Pi1 & Pi2 & Pi3
          Switch -.- Blue0
          Pi1 --- K[ssd]
          Pi2 --- L[ssd]
          Pi3 --- M[ssd]
          Blue0 --- O[ssd]
        end
      end
    end
  end
  A(Internet) --- Router
  A --- Workstation
  A --- B(Managed Cluster)
  Router --- Pi0
```

