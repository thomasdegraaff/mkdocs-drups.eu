# Tutorial

## Step 1. Initialize your project

When a Drups compatible cluster is running you have to initialize a new project to start using it.

1. Create a project folder.
```
mkdir your/project/folder
```
1. Get the code.
```
cd your/project/folder
git clone https://gitlab.com/thomasdegraaff/drups.git main
cd main
```
1. Create a remote git repository for the project (in Gitea or Gitlab or Github). Replace the git remote in your project main folder with the remote of that project repo, and push the code.
``` title="public repository"
git remote rm origin
git remote add origin https://git.your.domain/you/yourproject.git
git push -u origin main
```
``` title="private repository"
git remote rm origin
git remote add origin git@git.your.domain:you/yourproject.git
git push -u origin main
```
1. Set correct defaults in the ```drups-defaults``` file.
``` title="drups-defaults"
# Edit to change drups defaults.

# General.
default_client='tom'
default_domain='tomio.nl'

# Lando secrets.
lando_drupal_admin_password='drupal'
lando_drupal_cron_key='secret'
lando_drupal_hash_salt='secret'
lando_mariadb_password='drupal'

# Basic authentication (leave empty to disable basic authentication).
basic_authentication_user=''
basic_authentication_password=''

# Mailhog (when left empty mailhog will not be deployed remotely).
mailhog_user=''
mailhog_password=''

# Storage sizes.
code_storage_size='1Gi'
files_storage_size='1Gi'
database_storage_size='1Gi'

# Php settings.
php_memory_limit_build='256M'
php_memory_limit_tooling='256M'
php_memory_limit_fpm='256M'

php_max_execution_time_build='0'
php_max_execution_time_tooling='0'
php_max_execution_time_fpm='60'

php_upload_max_filesize_build='8M'
php_upload_max_filesize_tooling='8M'
php_upload_max_filesize_fpm='8M'

php_post_max_size_build='8M'
php_post_max_size_tooling='8M'
php_post_max_size_fpm='8M'

# Varnish.
varnish_size='400M'

# Xdebug.
xdebug_port='9003'

# Content security policy.
content_security_policy="default-src 'none'; script-src 'self' 'unsafe-inline'; connect-src 'self'; img-src 'self' data:; style-src 'self' 'unsafe-inline'; font-src 'self'; manifest-src 'self'; base-uri 'self';"
```
1. Initialize Drups.
```
./drups-init
```
Some configuration questions will be asked. If you prefer performance over high availabillity choose the storage access mode ```ReadWriteOnce```. This will significantly improve storage performance because all storage using pods will be fixed to a specific cluster node so no network communication is needed for these pods to access storage. The downside of this choice is that when that node goes down, so does the deployment. At the end take a note of the secrets and site url's.
1. Starup you local development site.
```
lando start
```
Your remote site and local development site are now deployed and you should be able to visit the local and remote urls.
1. Commit and push initialization changes.
```
git add .
git commit -m "Drups init."
git push
```
1. Update remote.
```
lando update-remote
```
1. Get remote data.<br/>The local data files are out-of-date because the remote site is updated. Pulling the data now makes sure you have data corresponding with the latest commit.
```
lando pull-data-remote
```

You are now ready to create a feature branch.

## Step 2. Create a feature branch
So you have initialized Drups, and your main branch is deployed. Now it's time to start developing on a separate ```feature``` (or any other name) branch.

1. Get the code from your project git repository.
```
cd your/project/folder
git clone https://git.your.domain/you/yourproject.git feature
cd feature
git checkout -b feature
git push -u origin feature
```
1. Initialize Drups, and answer ```yes``` when the script asks if data should be imported from another deployment. Import data from the previously deployed ```main``` branch deployment.
```
./drups-init
```
1. Starup you local development site.
```
lando start
```
Your remote site and local feature branch development site are now deployed and you should be able to visit the local and remote urls.

Now start developing.

## Step 3. Develop
1. Add a module.
```
lando composer require drupal/devel
lando drush pm:enable devel devel_generate
```
1. Create some site content.
```
lando drush devel-generate:content
```
1. Enable a theme.
```
lando drush theme:enable olivero
lando config-export
```
1. Edit ```drupal/config/sync/system.theme.yml``` and set ```olivero``` theme as default.
``` title="drupal/config/sync/system.theme.yml"
_core:
  default_config_hash: fOjer9hADYYnbCJVZMFZIIM1azTFWyg85ZkFDHfAbUg
admin: seven
default: olivero
```
1. Import the changed config into your local deployment.
```
lando config-import
```

If you're done, push your changes.

## Step 4. Push changes
You have created a ```feature``` branch, did some development, and want to update your remote feature branch deployment with these changes.

1. Export the Drupal configuration from your local development environment.
```
cd your/project/folder/feature
lando config-export
```
1. Commit and push the changes.
```
git add .
git commit -m "New feature."
git push
```
1. Update remote deployment.
```
lando update-remote
```

It's nice to showoff the new feature to the client with some content, so let's push local content to the remote deployment.

1. Store the data from the local Lando deployment.
```
lando pull-data-local
```
1. Push the data to the remote deployment.
```
lando push-data-remote
```

You can now demonstrate the new feature to the client in the remote deployment. Merge changes with the main branch when the client is happy.

## Step 5. Merge changes
Your client likes the new feature, so it's time to deploy the feature to the main branch.

1. Merge and push the changes to the ```main``` branch.
```
cd your/project/folder/feature
git checkout main
git pull --rebase
git merge --no-commit feature
git push
git checkout feature
```
1. Update the ```main``` branch deployment.
```
cd your/project/folder/main
git pull
lando update-remote
```

Your live deployment (main branch) is now updated with the new feature.

Now you know the basics, checkout [lando commands](./lando-commands.md) to become a power user.
