# Digital Ocean

## Create cluster

1. Create a Digital Ocean account.
```
https://cloud.digitalocean.com/registrations/new
```
1. Install the Digital Ocean commandline client ```doctl```: [instructions](https://github.com/digitalocean/doctl).
1. Create a Digital Ocean access token: [instructions](https://docs.digitalocean.com/reference/api/create-personal-access-token/).
1. Enable doctl authentication with the created access token.
```
$ doctl auth init
```
1. Once logged in to the Digital Ocean website click Kubernetes in the sidebar, and click 'Create a Kubernetes Cluster'.
![Create cluster](./images/do-create-1.png)
1. Check the Kubernetes version installed on your workstation.
```
kubectl version -o json | jq .
```
``` json
{
  "clientVersion": {
    "major": "1",
    "minor": "22",
    "gitVersion": "v1.22.3",
    "gitCommit": "c92036820499fedefec0f847e2054d824aea6cd1",
    "gitTreeState": "clean",
    "buildDate": "2021-10-27T18:41:28Z",
    "goVersion": "go1.16.9",
    "compiler": "gc",
    "platform": "linux/amd64"
  },
  "serverVersion": {
    "major": "1",
    "minor": "21",
    "gitVersion": "v1.21.9+k3s1",
    "gitCommit": "101917b0c493dd1effac1074feb1d5462b9a189b",
    "gitTreeState": "clean",
    "buildDate": "2022-01-25T00:51:36Z",
    "goVersion": "go1.16.10",
    "compiler": "gc",
    "platform": "linux/arm64"
  }
}
```
Make sure your Kubernetes client version differs no more than 1 version from the Kubernetes version you choose in the following screen. Set the other options matching your needs.
![Create cluster](./images/do-create-2.png)
![Create cluster](./images/do-create-3.png)
![Create cluster](./images/do-create-4.png)
1. Now continue with the 'Getting started' step 2.
![step 2](./images/do-create-5.png)
```
$ doctl kubernetes cluster kubeconfig save e1ac1693-aa42-4414-b2b1-3abdd1c866a4
Notice: Adding cluster credentials to kubeconfig file found in "/home/tdgraaff/.kube/config"
Notice: Setting current-context to do-ams3-k8s-1-21-11-do-0-ams3-1649486742685
```
1. Continue with step 3.
![step 3](./images/do-create-6.png)
1. Continue with step 4. Install Nginx Ingress Controller and Cert-Manager.
![step 4](./images/do-create-7.png)
1. Done with the Digital Ocean cluster installer.
![step 5](./images/do-create-8.png)
1. Check if the cluster is running.
``` bash
$ kubectx
default
do-ams3-k8s-1-21-11-do-0-ams3-1649486742685
$ kubectl get node
NAME                   STATUS   ROLES    AGE   VERSION
pool-4mcke2mua-cmqgp   Ready    <none>   29m   v1.21.11
pool-4mcke2mua-cmqgs   Ready    <none>   29m   v1.21.11
```
1. Create ```cluster-issuer.yaml``` with the following content.
``` yaml
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: letsencrypt-production
  namespace: default
spec:
  # ACME issuer configuration
  # `email` - the email address to be associated with the ACME account (make sure it's a valid one)
  # `server` - the URL used to access the ACME server’s directory endpoint
  # `privateKeySecretRef` - Kubernetes Secret to store the automatically generated ACME account private key
  acme:
    email: email@your.domain
    server: https://acme-v02.api.letsencrypt.org/directory
    privateKeySecretRef:
      name: letsencrypt-nginx-private-key
    solvers:
      # Use the HTTP-01 challenge provider
      - http01:
          ingress:
            class: nginx
```
1. Create the cluster issuer in the Kubernetes cluster so Let's encrypt certificates will be automaticly created when Drups creates a new site.
```
$ kubectl apply -f cluster-issuer.yaml
clusterissuer.cert-manager.io/letsencrypt-production created
```
Now your Digital Ocean Kubernetes cluster is initialized for Drups.

## Cluster access
For visitors to find the sites you deploy you need to configure DNS so the site domain name points to the cluster load balancer external ip addres.

To get the cluster load balancer external ip address.
```
$ kubectl get svc -n ingress-nginx | grep -E 'EXTERNAL-IP|LoadBalancer'
NAME                                 TYPE           CLUSTER-IP       EXTERNAL-IP     PORT(S)                      AGE
ingress-nginx-controller             LoadBalancer   10.245.7.36      138.68.122.98   80:31695/TCP,443:30452/TCP   40m
```
Configure dns at your registrars site.
![Dns settings](./images/do-dns-1.png)
![Dns settings](./images/do-dns-2.png)

Now your cluster is running you can start using it for Drupal development.

[Usage](/usage/intro){ .md-button }
