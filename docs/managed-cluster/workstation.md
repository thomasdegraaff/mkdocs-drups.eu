# Workstation preparation

Install the following software on your workstation:

* Kubectl: [instructions](https://kubernetes.io/docs/tasks/tools/)
* K9s: [instructions](https://k9scli.io/topics/install/)
* Kubectx and Kubens: [instructions](https://github.com/ahmetb/kubectx)
* Docker community edition (Needed for Lando): [instructions](https://docs.docker.com/get-docker/)
* Lando: [instructions](https://lando.dev/download/)

Now [continue](/managed-cluster/digital-ocean) with the installation of a Digital Ocean managed cluster.