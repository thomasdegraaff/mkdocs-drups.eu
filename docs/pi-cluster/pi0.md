# Install Pi0

The Pi0 sits between the Kubernetes cluster and the local network.

## Software overview

* [PiVPN](https://www.pivpn.io/): Provides access from anywhere to the cluster via VPN.
* [Nginx](https://www.nginx.com/): Distribute incoming internet traffic to the services running in the cluster.
* [Minio](https://min.io/): Provides object storage for cluster backups.
* [ddclient](https://github.com/ddclient/ddclient): Provides dhcp services for the cluster.
* [ics-dhcp-server](https://www.isc.org/dhcp/): Serves ip-addresses to the cluster nodes, and routes traffic into the cluster.

## Software installation

We will be using Ansible to install and configure the software on the pi0 and make it work as the cluster router. Connect the Pi0 with the ethernet adapter (eth1) to your local network so Ansible can connect to it. Make sure the internal network interface (eth0) is not connected to the local network! We will install a dhcp server on pi0 that would conflict with the dhcp server of the local network router.

> The usb Linksys ethernet adapter I used has an issue, it needs to be manually unplugged and plugged after reboot for it to work. This will be fixed when installing the software, but for now, make sure the ethernet adapter device functions, and if not, manually unplug/plug it.

``` mermaid
graph TB
  subgraph Local network
    Router --- Workstation
      EA[Ethernet adapter] --- Pi0
  end
  A((Internet)) --- Router
  Router --- EA
```

1. Find the ip address of the pi0.
```
sudo nmap -sP 192.168.10.0/24 | grep -P -B 2 '[ethernet adapter mac address]'
```
    Take a note of the ip-address and the mac address of the ethernet adapter.

2. Check if you can ssh into the pi0 without typing a password. (Use the ip found with the former action)
```
ssh pi@192.168.10.100
```

3. Download the Drups ansible playbook.
```
git clone https://gitlab.com/thomasdegraaff/drups-picluster
```

4. Install the playbook requirements ([bertvv.dhcp](https://galaxy.ansible.com/bertvv/dhcp) and [atosatto.minio](https://galaxy.ansible.com/atosatto/minio) roles).
```
cd drups-cluster-install
ansible-galaxy install -r requirements.yml
```

5. Copy ```inventory.yml.example``` to ```inventory.yml``` and edit ```inventory.yml``` to set the correct values. Set ```all:children:pi0:hosts``` to the pi0 ip address. And edit the variables under ```=== Pi0 install ===``` until ```=== Cluster install ===```.
```
cp inventory.yml.example inventory.yml
vim inventory.yml
```
    * The ```ddclient_conf``` settings vary depending on your [dynamic dns provider](https://github.com/ddclient/ddclient#supported-services). The example file contains [dynu.com](https://www.dynu.com/en-US/DynamicDNS) settings.

5. Run the playbook to install and configure the sofware on the pi0.
```
ansible-playbook -i inventory.yml pi0-install.yml
```

## Minio
You can now login to the Minio web ui. Use the ip address of the pi0 and the port configured in the ansible inventory as ```minio_server_addr```.

[http://192.168.192.1:9091](http://192.168.192.1:9091)

![Minio login screen](./images/minio-login.png)

Login with the ```minio_root_user``` and ```minio_root_password``` credentials set in the ansible inventory.

You should now see a bucket for the longhorn backups.

![Minio frontpage](./images/minio-frontpage.png)

And you should find a ```longhorn``` user.

![Minio user page](./images/minio-users.png)