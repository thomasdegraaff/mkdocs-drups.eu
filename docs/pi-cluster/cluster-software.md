# Install cluster software

* [Longhorn](https://longhorn.io) provides highly available persistent storage for Kubernetes.
* [Gitea](https://gitea.io/en-us/) is a community managed lightweight code hosting solution.
* [Cert manager](https://cert-manager.io/) provides [Let's Encrypt](https://letsencrypt.org/) certificate management for the sites running on the cluster.
* [NGINX ingress controller](https://kubernetes.github.io/ingress-nginx/) provides access to websites running in the cluster.
* [Docker pull through cache](https://docs.docker.com/registry/recipes/mirror/) provides a cache for docker images in the cluster.

## Installation
The cluster software is installed using Ansible. Connect to the cluster either by directly plugging your workstation into the cluster switch, or by activating the configured vpn connection on your workstation.

1. Check if you can access the cluster nodes.
```
ssh pi@pi1
ssh pi@pi2
ssh pi@pi3
ssh blue@blue0
```

1. Set correct values in the Ansible inventory under ```--- Cluster software install ---```.
```
    # Longhorn persisten storage.
    longhorn_ui_node_port: 31418 # Lonhorn ui cluster node port, range: 30000 - 32767.
    longhorn_backup_minio_user: longhorn
    longhorn_backup_minio_password: 'very-secure-secret'
    longhorn_backup_minio_bucket_name: longhorn-backup
    # Gitea.
    gitea_domain: git.your.domain
    gitea_database_password: 'very-secure-secret'
    gitea_username: 'username'
    gitea_email: 'username@your.domain'
    gitea_password: 'very-secure-secret'
    gitea_image: gitea/gitea:1.15.10
    # Cluster pull through Docker registry
    cluster_registry_http_node_port: 32555 # Port to access the registry within the cluster.
    registry_domain: registry.your.domain
    registry_password: 'very-secure-secret'
```

1. Run the ansible playbook.
```
ansible-playbook -i inventory.yml cluster-software-install.yml
```

1. Check if Longhorn is running properly.

    You should be able to visit the Longhorn ui by accessing one of the cluster nodes at the port set in the ```longhorn_ui_node_port``` inventory variable. For example open [http://192.168.192.101:31418](http://192.168.192.101:31418) in your webbrowser when you used the default settings.

    ![Longhorn frontpage](./images/longhorn-frontpage.png)


1. Check if Gitea is running properly.

    You should be able to login to Gitea running in the cluster by visiting the ```gitea_domain``` set in the inventory in your webbrowser. Use the ```gitea_username``` and ```gitea_password``` credentials fron the Ansible inventory to sign in.

    ![Gitea](./images/gitea-front.png)

That's it. Your Drups cluster is ready for usage now.

[Usage](/usage/intro){ .md-button }