# Prepare Blue0
## Install Ubuntu server

![Odyssey Bleu](./images/blue0.jpg){ align=right width="300"}


Install Ubuntu server on the Odyssey Blue. Click the link below for installation instructions.

[Ubuntu server installation instructions](https://ubuntu.com/server/docs/installation)

Steps

1. Create a bootable Ubuntu server usb drive.
2. Select the bootable usb as boot device.
    * Plug in your bootable USB drive, Monitor and keyboard into the Odyssey Blue, and power up. When the computer is booting, **keep pressing F7** to enter the Boot Manager Screen. And use ↑ and ↓ Key on keyboard to select your bootable USB drive.
3. Install Ubuntu.
    * Set username to 'pi'.
    * Set hostname to blue0.
    * Select 'Install ssh server'.

## Add public ssh key access

Connect the blue0 to the local network, and power on.

Scan the local network from your workstation to find the ip adress and mac address of the Odyssey Blue. In the example below 192.168.10.0/24 is a network subnet that contains the ip address range from 192.168.10.0 to 192.168.10.255. Change this range to fit your network. The grep command filters out the mac address ranges of Seeed Technology so you just get results for your Odyssey Blue, and not all the devices on your network.

```
sudo nmap -sP 192.168.10.0/24 | grep -P -B 3 '2C:F7:F1'
```

You will get something like this:

```
$ sudo nmap -sP 192.168.10.0/24 | grep -P -B 3 '2C:F7:F1'

MAC Address: 2C:F7:F1:12:43:51 (Realtek Semiconductor)
Nmap scan report for blue0 (192.168.10.204)
Host is up (0.00076s latency).
```
Note down the mac address.

Now enable ssh public key authentication on your Odyssey Blue.
```
ssh-copy-id pi@192.168.10.204
```
Check if you can access the Odyssey Blue without entering a password.
```
ssh pi@192.168.10.204
```

When you have accessed the Odyssey Blue execute the following commands to enable passwordless sudo for the pi user.
```
sudo echo 'pi ALL=(ALL) NOPASSWD: ALL' > /etc/sudoers.d/010_pi-nopasswd
sudo chmod 440 /etc/sudoers.d/010_pi-nopasswd
```
