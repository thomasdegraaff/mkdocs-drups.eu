# Enable cluster access

## Enable dynamic dns
When your internet provider gives you a new ip address, ddclient running on pi0 will notice. It will send a message to your dynamic dns provider containing your new ip address. When your dynamic dns provider receives this message it will update the ip address for your domain.

To make this work you need to register a domain, and use your dynamic dns provider as a nameserver for this domain.

1. Find your dynamic dns provider nameservers close to you. For dynu.com they can be found here: [https://www.dynu.com/en-US/DynamicDNS](https://www.dynu.com/en-US/DynamicDNS)
![Dynu nameservers](./images/dynu-nameservers.png)

2. Register a domain with a domain registrar, and make it use your dynamic dns providers nameservers. Configure a wildcard subdomain so all subdomains will be forwarded to the given ip address without further configuration.
![Registrar domain settings](./images/registrar-dns-0.png)
![Registrar domain settings](./images/registrar-dns-1.png)

3. Configure the given domain in your dynamic dns providers settings. For convenience I enable a wildcard dns record so any subdomain will be accepted without further configuration.
![Dynu settings](./images/dynu-settings.png)

4. Now check if your domain resolves properly to the external ip address of your local network router by executing the following command on your workstation.
```
host [your.domain]
```
This should give you an ip address.
```
$ host thomasdegraaff.eu
thomasdegraaff.eu has address 85.145.195.122
```
If this ip address equals the one [Google finds](https://www.google.com/search?q=what+is+my+ip) when connected to the local network all is set.

## Port forwarding

To access your cluster from the internet you need to forward some ports to the pi0. You can do this in your local network router settings. Look for 'Port forwarding'.

Look in the Ansible inventory file (```inventory.yml```) for the ports that need to be forwarded to the pi0.

```
pi0_http_proxy_port: 32080 # Pi0 incoming http traffic from local network.
pi0_https_proxy_port: 32443 # Pi0 incoming https traffic from local network.
pi0_gitea_ssh_proxy_port: 32022 # Pi0 incoming gitea ssh traffic.
pivpn_port: 49993
```

This gives the following port forwards:
```
service    router   Pi0    Protocol
-----------------------------------
http       80    -> 32080  tcp
https      443   -> 32443  tcp
gitea-ssh  22    -> 32022  tcp
vpn        49992 -> 49993  udp
```

In this example the pi0 local network address is 192.168.193.100:

![NAT settings](./images/nat-settings.png)

On pi0 a Nginx proxy server will forward the traffic from the incoming ports to the cluster nodes where configured Kubernetes services are listening.

## VPN access

1. Create a vpn client certificate.

    Ssh into pi0 and execute the following command to generate a ovpn client certificate for a vpn user as described in the [PiVPN documentation](https://docs.pivpn.io/openvpn/).
```
pivpn add
```
```
pi@pi0:~ $ pivpn add
Enter a Name for the Client:  pivpn-user
How many days should the certificate last?  1080
Enter the password for the client:
Enter the password again to verify:

...
...

========================================================
Done! pivpn-user.ovpn successfully created!
pivpn-user.ovpn was copied to:
  /home/pi/ovpns
for easy transfer. Please use this profile only on one
device and create additional profiles for other devices.
========================================================
```

2. Now copy the ```[chosen client name].ovpn```  file to your workstation.
```
scp pi@pi0:/home/pi/ovpns/pivpn-user.ovpn ./
```

3. Create a new vpn network connection on your workstation by importing this ovpn file with your network manager.
![Network manager](./images/network-manager-create.png)
![Network manager](./images/network-manager.png)

You can now access the cluster network from anywhere when using this vpn connection. You can test this by tethering your workstation over your smartphone using mobile internet (4G 5G). Enable the created vpn connection and try if you can ssh into your cluster.
```
ssh pi@pi0
```