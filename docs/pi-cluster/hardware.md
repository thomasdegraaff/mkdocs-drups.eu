# Hardware overview

> Raspberry pi's are hard to get at the moment. You can also use a [managed Kubernetes](/managed-cluster/workstation/) cluster.

An Anker powerport is used to power the four Raspberry Pi's (rpi's). An ethernet adapter provides a second ethernet port to one of the rpi's that is used as a router for the cluster. An ethernet switch connects the other rpi's and the Odyssey Blue nodes to this router. Each rpi has a ssd drive attached.

## Parts
* 4 x Raspberry Pi 4b
* 1 x (optional) - ODYSSEY Blue with Intel Celeron J4125 - 8GB RAM + 128GB SSD
* Anker Powerport 6
* 4 x flat cat. 7 ethernet cable 0.25 m
* 4 x Usb charging cable 0.3 m
* 2 x cat. 7 ethernet cable 0.5 m
* Netgear GS308 switch
* 4 x 128 GB 2.5 inch SSD
* 4 x SSD to USB 3.0 Cable
* C4labs 8-Slot Cloudlet Cluster Case
* TP-Link UE300 USB3 Gigabit ethernet adapter

## Diagram
``` mermaid
graph TB
  subgraph " "
    subgraph Local network
      Router --- Workstation
    end
    subgraph Drups hardware
      Pi0 --- N[ssd]
      EA[Ethernet adapter] --- Pi0[Pi0] --- Switch
      subgraph K3s Cluster
        Switch --- Pi1 & Pi2 & Pi3
        Switch -.- Blue0
        Pi1 --- K[ssd]
        Pi2 --- L[ssd]
        Pi3 --- M[ssd]
        Blue0 --- O[ssd]
      end
    end
  end
  A((Internet)) --- Router
  Router --- EA
```

## Photos

C4labs 8-Slot Cloudlet Cluster Case
![C4labs cluster front](./images/cluster-hw-front.jpg)
![C4labs cluster back](./images/cluster-hw-back.jpg)

4 layer stackable Raspberry Pi case with fan
![Stackable pi cluster](./images/cluster.jpg)