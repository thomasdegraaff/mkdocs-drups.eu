# Workstation preparation

Install the following software on your workstation:

* Ansible: [instructions](https://docs.ansible.com/ansible/latest/installation_instructions/index.html)
* Kubectl: [instructions](https://kubernetes.io/docs/tasks/tools/)
* K9s: [instructions](https://k9scli.io/topics/install/)
* Kubectx and Kubens: [instructions](https://github.com/ahmetb/kubectx)
* Raspberry Pi Imager: [instructions](https://www.raspberrypi.com/software/)
* Python 3 (needed for ansible)
``` bash
sudo apt install python3
pip install kubernetes
pip install openshift
ln -s /usr/bin/python3 /usr/bin/python
```
* Docker community edition (Needed for Lando): [instructions](https://docs.docker.com/get-docker/)
* Lando: [instructions](https://lando.dev/download/)
