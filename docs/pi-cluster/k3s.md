# Install K3s
## Intro

K3s is a lightweight implementation of kubernetes. This means the software running on server nodes to keep track of things running in the cluster doesn't use all server node resources. On Raspberry 4 server nodes enough free resources remain to deploy pods on the server nodes. The chosen setup has three server nodes to create a high availability cluster, and optionally one extra Odyssey Blue agent node. More nodes can be added when needed.

We will use an Ansible role based on the [k3s-io github repo](https://github.com/k3s-io/k3s-ansible) to install k3s on the cluster nodes. Make sure your workstation can access the cluster network, either by directly plugging into the cluster switch, or by accessing the cluster via vpn.

``` mermaid
graph TB
  subgraph " "
    subgraph Local network
      Router
    end
    subgraph Drups hardware
      EA[Ethernet adapter] --- Pi0 --- Switch
      subgraph K3s Cluster
        Switch --- Pi1 & Pi2 & Pi3
        Switch -.- Blue0
        subgraph K3s init server
          Pi1 --- K[ssd]
        end
        subgraph K3s server
          Pi2 --- L[ssd]
          Pi3 --- M[ssd]
        end
        subgraph K3s agent
          Blue0 --- O[ssd]
        end
      end
    end
    Workstation --- Switch
  end
  A((Internet)) --- Router
  Router --- EA
  WS[workstation]-- vpn ---A
```

If the previous steps were succesfull, your workstation now received an ip address in the ip range of the cluster network.

Check if you can access the rpi's (and blue0) without a password by ip-address.
```
ssh pi@192.168.192.101
ssh pi@192.168.192.102
ssh pi@192.168.192.103
ssh pi@192.168.192.120
```

## Install k3s

1. Edit ```inventory.yml``` in the Drups ansible playbook. Make sure there is one ```init server``` host, and a even number of ```server node``` hosts.
```
all:
  children:
    pi0:
      hosts: 192.168.192.1 # Either eth0 or eth1 ip address, depending network you're connected to.
    cluster_node:
      children:
        k3s_init_server_node:
          hosts:
            192.168.192.101:
        k3s_server_node:
          hosts:
            192.168.192.102:
            192.168.192.103:
        k3s_agent_node:
          hosts:
            192.168.192.120:
```

    Set the the ```k3s_version``` and ```k3s_token``` variables.
```
    k3s_version: v1.21.9+k3s1 # Check https://update.k3s.io/v1-release/channels.
    k3s_token: 'yai7fuz7Oof1lo4Ho5uyaiGh' # Set a unique token.
```

2. Install the K3s cluster.
```
ansible-playbook -i inventory.yml cluster-install.yml
```

3. Copy the K3s config from the master to your workstation so you get access to the cluster.
```
scp pi@192.168.192.101:~/.kube/config ~/.kube/config
```

4. Check if all nodes are ok.
```
kubectl get node
```

    The result should be something like this.
```
$ kubectl get node

NAME    STATUS   ROLES                       AGE   VERSION
blue0   Ready    <none>                      24m   v1.22.6+k3s1
pi1     Ready    control-plane,etcd,master   25m   v1.22.6+k3s1
pi2     Ready    control-plane,etcd,master   24m   v1.22.6+k3s1
pi3     Ready    control-plane,etcd,master   24m   v1.22.6+k3s1
```
