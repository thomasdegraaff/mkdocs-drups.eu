# Prepare Raspberry Pi's
## Install Raspberry Pi OS

![Raspberry Pi](./images/rpi.jpg){ align=right width="300"}

Install Raspberry Pi OS on the ssd drives. Check the link below for detailed installation instructions. Use the Raspberry Pi imager advanced menu to setup ssh access from your workstation using pasword authentication.

[Raspberry Pi website installation instructions](https://www.raspberrypi.com/documentation/computers/getting-started.html#using-raspberry-pi-imager)


Steps

* Insert the ssd drive in your workstation, and startup rpi-imager
``` bash
sudo rpi-imager
```
* Rpi-imager:

    1. Choose OS:
        * Raspberry Pi OS Lite (64-bit)

    2. Storage:
        * Select the ssd card for the corresponding rpi.

    3. Advanced options:
        * Hostname: pi0
        * Enable ssh
        * Use password authentication
        * Username: pi
        * Password: \*\*\*\*\*\*

    4. Repeat steps for the next Pi, changing only the hostname (pi1 - pi3).

Rpi-imager screens:

![](./images/rpi-imager-01.png)

![](./images/rpi-imager-02.png)

![](./images/rpi-imager-03.png)

![](./images/rpi-imager-04.png)

![](./images/rpi-imager-05.png)

## Add ssh public key access

Now connect the ssd drives with the rpi's, and connect the rpi's with your local network, and turn them on.

``` mermaid
graph TB
  subgraph Local network
    Router --- Workstation
      Switch --- Pi0 & Pi1 & Pi2 & Pi3
      Pi0 --- J[ssd]
      Pi1 --- K[ssd]
      Pi2 --- L[ssd]
      Pi3 --- M[ssd]
  end
  A((Internet)) --- Router
  Router --- Switch
```
Scan the local network from your workstation to find the ip adresses and mac addresses of the pi's. In the example below 192.168.10.0/24 is the router network subnet that contains the ip address range from 192.168.10.0 to 192.168.10.255. Change this range in the command below to fit your local network. The grep command filters out the mac address ranges of the Raspberry Pi foundation so you just get results for your rpi's, and not all the devices on your network.

```
sudo nmap -sP 192.168.10.0/24 | grep -P -B 2 'B8:27:EB|DC:A6:32|E4:5F:01'
```

You will get something like this:

```
$ sudo nmap -sP 192.168.10.0/24 | grep -P -B 2 'B8:27:EB|DC:A6:32|E4:5F:01'

MAC Address: 00:E0:4C:36:49:52 (Realtek Semiconductor)
Nmap scan report for pi0 (192.168.10.200)
Host is up (0.00076s latency).
MAC Address: E4:5F:01:2B:77:F5 (Unknown)
Nmap scan report for pi1 (192.168.10.201)
Host is up (0.00033s latency).
MAC Address: E4:5F:01:2B:77:F2 (Unknown)
Nmap scan report for pi2 (192.168.10.202)
Host is up (0.00033s latency).
MAC Address: E4:5F:01:2B:78:19 (Unknown)
Nmap scan report for pi3 (192.168.10.203)
Host is up (0.00031s latency).
MAC Address: E4:5F:01:2B:77:D4 (Unknown)
```
Note down the mac addresses and ip adresses for each pi.

Now enable ssh public key authentication on your pi's.
```
ssh-copy-id pi@192.168.10.200
ssh-copy-id pi@192.168.10.201
ssh-copy-id pi@192.168.10.202
ssh-copy-id pi@192.168.10.203
```
Check if you can access the pi's without entering a password. Note down the hostname for each pi so you know for each pi the mac address of the build in ethernet device.
```
ssh pi@192.168.10.200
ssh pi@192.168.10.201
ssh pi@192.168.10.202
ssh pi@192.168.10.203
```

When logged in to pi0 you need to find the mac address of the ethernet adapter device, and take a note of that.
```
ip addr show | grep -A 1 eth1
```