# Maintenance

## Storage backups
todo

## Update Gitea
todo

## Update K3s
todo

## Add domain to Dynamic DNS client
Ssh into the pi0 cluster router.
```
ssh pi@pi0
```
Edit ```/etc/ddclient.conf``` and add the domain in the last line of this file.
```
# Ansible managed

protocol=dyndns2
use=web, web=checkip.dynu.com/, web-skip='IP Address'
server=api.dynu.com
login=[username]
password=[secret]
thomasdegraaff.eu,tomio.nl,drups.eu
```