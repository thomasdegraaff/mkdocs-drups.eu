# Add cluster nodes

Connect the pi1-3 (and the blue0) with the pi0 according to the diagram below. Connect your workstation to the switch.

``` mermaid
graph TB
  subgraph " "
    subgraph Local network
      Router
    end
    subgraph Drups hardware
      EA[Ethernet adapter] --- Pi0 --- Switch
      subgraph K3s Cluster
        Switch --- Pi1 & Pi2 & Pi3
        Switch -.- Blue0
          Pi1 --- K[ssd]
          Pi2 --- L[ssd]
          Pi3 --- M[ssd]
          Blue0 --- O[ssd]
      end
    end
    Workstation --- Switch
  end
  A((Internet)) --- Router
  Router --- EA
```

If the [Install pi0](installatioin/pi0) step was succesfull, your workstation now received an ip address in the ip range of the cluster network. Check if the other cluster devices can be found, and if their ip addresses correspond to the addresses configured in the Ansible inventory.

```
sudo nmap -sP 192.168.192.0/24
```

You will get something like this.

```
$ sudo nmap -sP 192.168.192.0/24

Starting Nmap 7.80 ( https://nmap.org ) at 2022-02-08 18:14 CET
Nmap scan report for 192.168.192.101
Host is up (0.00020s latency).
MAC Address: E4:5F:01:2B:78:19 (Raspberry Pi Trading)
Nmap scan report for 192.168.192.102
Host is up (0.00020s latency).
MAC Address: E4:5F:01:2B:77:F2 (Raspberry Pi Trading)
Nmap scan report for 192.168.192.103
Host is up (0.00019s latency).
MAC Address: E4:5F:01:2B:77:F5 (Raspberry Pi Trading)
Nmap scan report for 192.168.192.120
Host is up (0.00034s latency).
MAC Address: 00:E0:4C:08:DE:FA (Realtek Semiconductor)
Nmap scan report for 192.168.192.1
Host is up.
Nmap done: 256 IP addresses (5 hosts up) scanned in 3.56 seconds
```

Your workstation should also have a working internet connection.

Check if you can access the rpi's (and blue0) without a password.
```
ssh pi@192.168.192.101
ssh pi@192.168.192.102
ssh pi@192.168.192.103
ssh pi@192.168.192.120
```

For convenience you can now add aliases for the cluster nodes to the ```/etc/hosts``` file on your workstation so you don't need to type the ip addresses every time.
```
127.0.0.1	localhost
127.0.1.1	tdgraaff-ThinkPad-X250

# The following lines are desirable for IPv6 capable hosts
::1     ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters

# Custom
192.168.192.1 pi0
192.168.192.101 pi1
192.168.192.102 pi2
192.168.192.103 pi3
192.168.192.120 blue0
```

To see if the aliases work check if you can access pi0.
```
ssh pi@pi0
```

