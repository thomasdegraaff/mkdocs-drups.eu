# Feedback
If you run into issues, have suggestions, want to improve code or have other questions or tips you can contact me at <a href="mailto:thomasdegraaff-office@protonmail.com">thomasdegraaff-office@protonmail.com</a>.

## Known issues
| Date | Issue | State |
| --- | --- | --- |
| 27-04-2022 | Local deployment misses security headers | To do |
| 06-06-2022 | lando cc-local broken | To do |

## Features
| Date | Feature | State |
| --- | --- | --- |
| 29-04-2022 | Set Varnish config when deploying, not in image build. This makes it easier to customize. | To do |
| 05-06-2022 | Secure used images. | To do |
| 08-06-2022 | Add lando command to update images (local or remote) | To do |
