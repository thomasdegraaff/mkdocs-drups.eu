---
hide:
  - navigation
  - toc
title: Drups
---
<style>
  .md-typeset h1,
  .md-content__button {
    display: none;
  }
</style>
# Introduction
[How to use Drups](/usage/intro){ .md-button }
[setup a managed cluster](/managed-cluster/workstation/){ .md-button }
[Build your Pi cluster](/pi-cluster/demo/){ .md-button }
