---
hide:
  - navigation
  - toc
title: Drups | build your own Drupal devops environment
---
<style>
  .md-typeset h1,
  .md-content__button {
    display: none;
  }
</style>
## Presentation
A short introduction into Drups. What is the aim of Drups, how does it work, how do you set it up.
<div style="max-width:800px"><div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/703237892?h=d6ee0174a7&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;" title="drups-intro"></iframe></div><script src="https://player.vimeo.com/api/player.js"></script></div>

## The idea
To build and configure your own Drupal devops environment is a lot of work. Drups makes this a breeze by using available open source tools to automate the setup and configuration of your own Drupal devops environment.

[Lando](https://lando.dev/) is a great project for local Drupal development. It is a wrapper around Docker that makes it really easy to spin up the needed containers and development tooling on your workstation.

[Kubernetes](https://kubernetes.io/) makes it easy to spin up containers in a controlled cluster environment and publish services on the internet.

Drups connects these two tools to create a convenient Drupal development experience. Develop a project branch on your workstation, and then publish it in a Kubernetes cluster for reviewing, testing, production etc.

You can build your own cluster using cheap Raspberry Pi hardware in three hours. You can setup a third party managed Kubernetes cluster solution in 10 minutes. Or do both.

## Features
* Develop on your local workstation anywhere.
* Multi cluster support, use separate clusters for your development and live deployments.
* Easily deploy your site's branches in your cluster and publish them on the internet.
* Exactly the same server environment for local development and cluster deployments.
* Drupal caching configured and enabled out of the box (Varnish, Redis, Opcache).
* Secure out of the box (Letsencrypt certificates and secure headers configured).
* Site storage backups and restores.
* Use a git workflow to work together on projects in a team.
* Development tools configured (Xdebug, phpstan, codesniffer).
* High availability, one cluster node (Raspberry Pi) down (for maintenance), other nodes will take over.
* Completely open source, and easily customizable with basic linux knowledge.

[How to use Drups](/usage/intro){ .md-button }
[setup a managed cluster](/managed-cluster/workstation/){ .md-button }
[Build your Pi cluster](/pi-cluster/demo/){ .md-button }